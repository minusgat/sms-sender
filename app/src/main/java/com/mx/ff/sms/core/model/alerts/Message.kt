package com.mx.ff.sms.core.model.alerts

data class Message(
    val coordinates: Coordinates,
    val dn: Long,
    val email: String,
    val message: String,
    val time: String
)