package com.mx.ff.sms.core.net;

import android.util.Log;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

import static com.mx.ff.sms.core.net.Routes.SERVER_URL;


public class SocketConnection {
    private static final String TAG = SocketConnection.class.getSimpleName();
    private static final String TOKEN = "token";
    private static final SocketConnection socketConnection = new SocketConnection();
    private Socket socket;


    public static SocketConnection getInstance() {
        return socketConnection;
    }

    public void init() {
        try {
            socket = IO.socket(SERVER_URL);
            Log.e(TAG, "Iniciando en " + SERVER_URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public Boolean isConnected() {
        return socket.connected();
    }
}

