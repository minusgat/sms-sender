package com.mx.ff.sms.core.model.alerts

data class Messages(
    val messages: List<Message>
)