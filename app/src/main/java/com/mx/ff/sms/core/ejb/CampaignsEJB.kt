package com.mx.ff.sms.core.ejb


import android.util.Log
import com.google.gson.Gson
import com.mx.ff.sms.core.model.Message
import com.mx.ff.sms.core.model.MessageBlock
import com.mx.ff.sms.core.net.Routes.*
import com.mx.ff.sms.core.net.WebService
import com.mx.ff.sms.core.util.AsyncResponse
import com.mx.ff.sms.core.util.Response
import com.mx.ff.sms.core.util.ResponseCode
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.http.PATCH
import retrofit2.http.POST

class CampaignsEJB(viewUpdater: AsyncResponse) : CustomEJB(viewUpdater) {
    private val TAG = CampaignsEJB::class.java.simpleName

    companion object {
        private const val BASE_URI = "campaigns"
        private const val TRANSACTIONS = "transactions"
        private const val PLATAFORM_ID = "platformId"
        private const val CREATION_DATE = "creationDate"
        private const val TYPE = "type"
    }


    fun retriveMessagesBlocks() {
        val data = JSONObject()
        data.put(PLATAFORM_ID,1)

        WebService(
                retriveMessageBlocksCallback,
                GET_METHOD,
                "$BASE_URI"
        ).execute(data)
    }

    private val retriveMessageBlocksCallback = AsyncResponse { response: Response<*> ->
        val forward: Response<ArrayList<MessageBlock>> = response.convert()
        val transactionList = ArrayList<MessageBlock>()
        forward.data = transactionList
        try {
            if (response.isOk) {
                val transactionsJsonList: JSONArray = ((response.data) as JSONObject).getJSONArray(TRANSACTIONS)
                for (x in 0 until transactionsJsonList.length())
                    transactionList.add(Gson().fromJson(transactionsJsonList.get(x).toString(), MessageBlock::class.java))
            } else response.makeError()
        } catch (e: Exception) {
            response.makeError(ResponseCode.UNKNOWN_ERROR, e.message, TAG)
            e.printStackTrace()
            Log.e(TAG, response.toString())
        }
        if (validateViewUpdater()) viewUpdater.processFinish(forward)
    }

    fun retriveMessagesBlocksDetail(creationDate :String) {

        val data = JSONObject()
        data.put(PLATAFORM_ID,1)

        WebService(
                retriveMessageBlockDetailCallback,
                GET_METHOD,
                "$BASE_URI/$creationDate"
        ).execute(data)
    }

    private val retriveMessageBlockDetailCallback = AsyncResponse { response: Response<*> ->
        val forward: Response<ArrayList<Message>> = response.convert()
        val messageList = ArrayList<Message>()
        forward.data = messageList
        try {
            if (response.isOk) {
                val messageJsonList: JSONArray = ((response.data) as JSONObject).getJSONArray(TRANSACTIONS)
                for (x in 0 until messageJsonList.length())
                    messageList.add(Gson().fromJson(messageJsonList.get(x).toString(), Message::class.java))
            } else response.makeError()
        } catch (e: Exception) {
            response.makeError(ResponseCode.UNKNOWN_ERROR, e.message, TAG)
            e.printStackTrace()
            Log.e(TAG, response.toString())
        }
        if (validateViewUpdater()) viewUpdater.processFinish(forward)
    }

    fun setMessageStatus(messageId :String,status:String) {

        val data = JSONObject()
        data.put(TYPE,status)

        WebService(
                basicCallbck,
                PATCH_METHOD,
                "sms/$messageId"
        ).execute(data)
    }

    val basicCallbck = AsyncResponse { response: Response<*> ->
        if (!response.isOk) response.makeError()
        if (validateViewUpdater()) viewUpdater.processFinish(response)
    }

}