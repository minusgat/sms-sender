package com.mx.ff.sms.core.util;

/**
 * Catalog of Response codes
 */

public class ResponseCode {
    public static final int OK = 200;
    public static final int CONVERSION_ERROR = -4;
    public static final int JSON_ERROR = -3;
    public static final int UNKNOWN_ERROR = -2;
    public static final int UNEXPECTED_ERROR_CODE = -1;

    public static final int DEFAULT_ERROR = 0;
    public static final int MISSING_REQUIRED_DATA = 2;
    public static final int SIGN_IN_ERROR = 3;
}
