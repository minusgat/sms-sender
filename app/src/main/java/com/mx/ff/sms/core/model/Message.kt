package com.mx.ff.sms.core.model

data class Message(
    val attempts: Int,
    val clickedDate: String,
    val dn: Long,
    val message: String,
    val publicId: String,
    val receivedDate: String,
    val sendDate: String,
    val username: String
)