package com.mx.ff.sms.view.activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mx.ff.sms.adapter.MessagesAdapter;
import com.mx.ff.sms.core.model.TextMessage;
import com.mx.ff.sms.core.ejb.CampaignsEJB;
import com.mx.ff.sms.core.ejb.MessagesEJB;
import com.mx.ff.sms.R;

import java.util.ArrayList;
import java.util.Locale;

import static com.mx.ff.sms.core.Constantes.DELIVERED_MESSAGES;
import static com.mx.ff.sms.core.Constantes.SENT_MESSAGES;
import static java.lang.String.format;

public class MainActivity extends AppCompatActivity {

    private static final long START_TIME_IN_MILLIS = 9000;
    public TextView inCommingTextView, outGoingTextView, counter, totalTextView, enviadosTextview, entregadosTextview;
    private MessagesAdapter messagesAdapter, adapter;
    private CountDownTimer mCountDownTimer;
    private boolean mTimerRunning;
    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;
    private RelativeLayout totalContainer, enviadoscontainer, recibidosContainer;
    private final ArrayList<TextMessage> sentMessagesArraylist = new ArrayList<>();
    private final ArrayList<TextMessage> deliveredMessagesArraylist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 301);
        }


        RecyclerView listaPosts = findViewById(R.id.a_main_incomming_recycler_view);
        RecyclerView recyclerView = findViewById(R.id.a_main_out_going_recycler_view);
        inCommingTextView = findViewById(R.id.a_main_out_going_text_view);
        outGoingTextView = findViewById(R.id.textView2);
        counter = findViewById(R.id.counter);
        totalTextView = findViewById(R.id.a_main_total_textview);
        enviadosTextview = findViewById(R.id.a_main_enviados_text_viados);
        entregadosTextview = findViewById(R.id.a_main_entregados_text_view);
        totalContainer = findViewById(R.id.a_main_total_container);
        enviadoscontainer = findViewById(R.id.a_main_enviados_container);
        recibidosContainer = findViewById(R.id.a_main_recibidos_container);


        listaPosts.setLayoutManager(new LinearLayoutManager(this));
        messagesAdapter = new MessagesAdapter(this, listaPosts);
        listaPosts.setAdapter(messagesAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MessagesAdapter(this, recyclerView);
        recyclerView.setAdapter(adapter);


    }

    public void sendSMS(TextMessage mesage) {
        String SENT = "SMS_SENT_" + mesage.getId();
        String DELIVERED = "SMS_DELIVERED_" + mesage.getId();

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String status = "";
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        status = "Enviado";
                        updateSentMesage(mesage, SENT_MESSAGES);
                        sentMessagesArraylist.add(mesage);
                        enviadosTextview.setText(sentMessagesArraylist.size() + "");

                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        status = "Generic Failure";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        status = "No Service";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        status = "Null PDU";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        status = "Radio off";
                        break;
                }
                Log.e("Estatus", status);
                Toast.makeText(MainActivity.this, status, Toast.LENGTH_SHORT).show();

            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String status = "";
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        status = "Entregado";
                        updateSentMesage(mesage, DELIVERED_MESSAGES);
                        deliveredMessagesArraylist.add(mesage);
                        entregadosTextview.setText(deliveredMessagesArraylist.size() + "");
                        break;
                    case Activity.RESULT_CANCELED:
                        status = "No Entregado";
                        break;
                }

                Toast.makeText(MainActivity.this, status, Toast.LENGTH_SHORT).show();
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> partes = new ArrayList<>();

        sms.sendTextMessage(mesage.getNumber(), null, mesage.getMessages().toUpperCase(), sentPI, deliveredPI);

    }

    public void startTimer(View view) {
        enableCount();
    }

    public void enableCount() {
        if (messagesAdapter.getMessages().size() > 0) {
            TextMessage textMessage = messagesAdapter.removeMessage();
            updateCounter(messagesAdapter.getItemCount());
            sendSMS(textMessage);
            mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mTimeLeftInMillis = millisUntilFinished;
                    updateCountDownText();
                }

                @Override
                public void onFinish() {
                    mTimerRunning = false;
                    resetTimer();
                }
            }.start();
            mTimerRunning = true;
        } else Toast.makeText(this, "No hay mensajes para mandar", Toast.LENGTH_SHORT).show();
    }

    private void pauseTimer() {
        mCountDownTimer.cancel();
        mTimerRunning = false;
    }

    private void resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS;
        updateCountDownText();
        enableCount();

    }

    private void updateCountDownText() {
        int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;
        String timeLeftFormatted = format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        counter.setText(timeLeftFormatted);
    }

    public void getAllMessages() {
        new MessagesEJB(r -> {
            if (r.isOk()) {
                messagesAdapter.bindMessages((ArrayList<TextMessage>) r.getData());
                updateCounter(messagesAdapter.getItemCount());
            }

        }, this).getMessages(0);
    }

    public void getSentMessages() {
        new MessagesEJB(r -> {
            if (r.isOk())
                messagesAdapter.bindMessages((ArrayList<TextMessage>) r.getData());
        }, this).getMessages(1);
    }

    public void getNotSentMessages() {
        new MessagesEJB(r -> {
            if (r.isOk())
                messagesAdapter.bindMessages((ArrayList<TextMessage>) r.getData());
            updateCounter(messagesAdapter.getItemCount());
        }, this).getMessages(2);
    }

    public void getDeliveredMessages() {
        new MessagesEJB(r -> {
            if (r.isOk())
                messagesAdapter.bindMessages((ArrayList<TextMessage>) r.getData());
        }, this).getMessages(2);
    }

    public void getMessagesBlocks() {
        new CampaignsEJB(r -> {
            if (r.isOk())
                Toast.makeText(this, "Ok", Toast.LENGTH_SHORT).show();
        }).retriveMessagesBlocks();
    }

    public void updateSentMesage(TextMessage textMessage, String option) {
        new MessagesEJB(r -> {
            if (r.isOk())
                Toast.makeText(this, "Actualizado Status", Toast.LENGTH_SHORT).show();
            enviadosTextview.setText(format(Locale.getDefault(), "Enviados (%d)", sentMessagesArraylist.size()));
            entregadosTextview.setText(format(Locale.getDefault(), "Entregados (%d)", deliveredMessagesArraylist.size()));
        }, this).updateMessages(textMessage.getId(), option);
    }

    public void updateCounter(int count) {
        totalTextView.setText(format(Locale.getDefault(), "Total (%d)", count));
    }

    public void showView(View view) {
        if (view.getId() == R.id.a_main_chat_tab) {
            totalContainer.setVisibility(View.VISIBLE);
            recibidosContainer.setVisibility(View.GONE);
            enviadoscontainer.setVisibility(View.GONE);
        } else if (view.getId() == R.id.a_main_enviados_tab) {
            totalContainer.setVisibility(View.GONE);
            recibidosContainer.setVisibility(View.GONE);
            enviadoscontainer.setVisibility(View.VISIBLE);

        } else {
            totalContainer.setVisibility(View.GONE);
            enviadoscontainer.setVisibility(View.GONE);
            recibidosContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMessagesBlocks();
    }
}
