package com.mx.ff.sms.core.model

data class Campaign(
    val isToSend: String,
    val seconds: String
)