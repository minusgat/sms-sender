package com.mx.ff.sms.core;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.mx.ff.sms.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



public class Utilidades {

    public static JSONObject generaJSON(ArrayList<String[]> params) throws JSONException {
        JSONObject obj = new JSONObject();
        for (String[] param : params) {
            obj.put(param[0], param[1]);
        }
        return obj;
    }

    public static JSONObject generaJSONSinLaves(String[]... params) throws JSONException {
        JSONObject obj = new JSONObject();
        for (String[] param : params) {
            obj.put(param[0], param[1]);
        }
        return obj;
    }

    public static void showNotification(String message, String title, Context context, Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT
        );

        Notification notification = new NotificationCompat.Builder(context, "Mensajeria")
                .setSmallIcon(R.drawable.message_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setVibrate(new long[]{0,1800,500,1200,100,200,100,200})
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .build();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(8, notification);
    }


}
