package com.mx.ff.sms.core.ejb;

import android.app.Activity;
import android.util.Log;

import com.mx.ff.sms.core.model.TextMessage;
import com.mx.ff.sms.core.net.Routes;
import com.mx.ff.sms.core.net.SocketConnection;
import com.mx.ff.sms.core.util.AsyncResponse;
import com.mx.ff.sms.core.util.Response;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.mx.ff.sms.core.net.Routes.SEND_SMS_BLOCK;


public class ChatEJB extends CustomEJB {

    public static final String ON_CONNECTION = "onConnection";
    public static final String ON_DISCONNECT = "onDisconnect";
    public static final String ON_ENVIA_SMS = "enviaSMS ";
    public static final String ON_ENVIA_SMS_BLOCK = "onEnviaSMSblock";
    private static final String TAG = ChatEJB.class.getSimpleName();
    private Activity act;
    private Socket socket;
    private String token;

    public ChatEJB(AsyncResponse viewUpdater, Activity activity, String token) {
        super(viewUpdater);
        this.act = activity;
        this.token = token;
    }

    public void connect() {
        if (SocketConnection.getInstance().getSocket() == null)
            SocketConnection.getInstance().init();
        socket = SocketConnection.getInstance().getSocket();
        socket.on(Socket.EVENT_CONNECT, this::onConnect);
        socket.on(Socket.EVENT_DISCONNECT, this::onDisconnect);
        socket.on(Routes.SEND_SMS, this::onSendSms);
        socket.on(Routes.SEND_SMS_BLOCK, this::onSendSmsBlock);
        socket.on(SEND_SMS_BLOCK, this::onSendSmsBlock);
        socket.connect();
    }

    private void onConnect(Object... args) {
        Log.e(TAG, "onConnect: ");
        Response<String> res = new Response<>();
        res.setMessage("OnConnect");
        Response<String> forward = res.convert();
        act.runOnUiThread(() -> {
            if (validateViewUpdater()) viewUpdater.processFinish(forward);
        });
    }

    private void onConnection(Object... objects) {
        Response<String> res = new Response<>();
        res.setMessage(ON_CONNECTION + objects[0].toString());
        Response<String> forward = res.convert();
        act.runOnUiThread(() -> {
            if (validateViewUpdater()) viewUpdater.processFinish(forward);
        });
    }

    public void pauseEmmiters() {
        socket.off(Socket.EVENT_CONNECT);
        socket.off(Socket.EVENT_DISCONNECT);
        socket.off(Routes.SEND_SMS);

    }

    public void resumeEmmiters() {
        socket.on(Socket.EVENT_CONNECT, this::onConnect);
        socket.on(Socket.EVENT_DISCONNECT, this::onDisconnect);
        socket.on(Routes.SEND_SMS, this::onConnection);

    }

    public void disconnect() {
        socket.off(Socket.EVENT_CONNECT);
        socket.off(Socket.EVENT_DISCONNECT);
        socket.off(Routes.SEND_SMS);
        socket.disconnect();
    }

    private void onDisconnect(Object... args) {
        Response<String> res = new Response<>();
        res.setMessage(ON_DISCONNECT);
        Response<String> forward = res.convert();
        act.runOnUiThread(() -> {
            if (validateViewUpdater()) viewUpdater.processFinish(forward);
        });
    }

    private void onSendSms(Object... args) {
        try {
            Log.e(TAG, ON_ENVIA_SMS + " " + args[0].toString());
            Response<TextMessage> forward = new Response<>();
            JSONObject jsonObject = (JSONObject) args[0];
            TextMessage textMessage = new TextMessage();
            textMessage.setMessages(jsonObject.getString("texto"))
                    .setNumber(jsonObject.getString("telefono"))
                    .setFecha(jsonObject.getString("date"));
            forward.setData(textMessage);
            forward.setMessage(ON_ENVIA_SMS);
            act.runOnUiThread(() -> {
                if (validateViewUpdater()) viewUpdater.processFinish(forward);
            });
        } catch (Exception e) {
        }
    }

    private void onSendSmsBlock(Object... args) {
        try {
            Log.e(TAG, SEND_SMS_BLOCK + " " + args[0].toString());
            Response<ArrayList<TextMessage>> forward = new Response<>();
            JSONArray jsonArray = (JSONArray) args[0];
            ArrayList<TextMessage> textMessageArrayList =new ArrayList<>();
            for (int i=0;i<jsonArray.length();i++)
                textMessageArrayList.add(TextMessage.fromJson((JSONObject) jsonArray.get(i)).getData());
            forward.setData(textMessageArrayList);
            forward.setMessage(ON_ENVIA_SMS_BLOCK);
            act.runOnUiThread(() -> {
                if (validateViewUpdater()) viewUpdater.processFinish(forward);
            });
        } catch (Exception e) {
        }
    }
}
