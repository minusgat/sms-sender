package com.mx.ff.sms.view.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.mx.ff.sms.R
import com.mx.ff.sms.core.ejb.UserEJB
import com.mx.ff.sms.core.util.AsyncResponse
import com.mx.ff.sms.core.util.SMSService
import com.mx.ff.sms.core.util.Sesion
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private var SEND_SMS_PERMISSIONS = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        Sesion.getSesion().init(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        requestSendSmsPermissions()
        startService(Intent(this, SMSService::class.java))
        a_login_username_textview.setText(Sesion.getSesion().userName)
        a_login_password_textview.setText(Sesion.getSesion().password)


    }

    fun requestSendSmsPermissions() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permisos Malformados", Toast.LENGTH_SHORT).show()
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.SEND_SMS,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECEIVE_SMS), SEND_SMS_PERMISSIONS)

        } else {
            /*UserEJB(AsyncResponse {
                if (it.isOk)
                    startActivity(Intent(this, BlocksActivity::class.java))
                else Toast.makeText(this, "Ups! Something was wrong", Toast.LENGTH_SHORT).show()

            }).getToken()*/
        }
    }


    fun login(view: View) {

        val username = a_login_username_textview.text.toString()
        val password = a_login_password_textview.text.toString()
        a_login_progress_bar.visibility = View.VISIBLE
        UserEJB(AsyncResponse {
            a_login_progress_bar.visibility = View.GONE
            if (it.isOk) {
                Sesion.getSesion().userName = username
                Sesion.getSesion().password = password
                startActivity(Intent(this, SenderActivity::class.java))
            } else Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()

        }).getToken(a_login_username_textview.text.toString(), a_login_password_textview.text.toString())
    }
}
