package com.mx.ff.sms.core.util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Generic responses, shared between the process
 */

public class Response<T> {
    public static final String JSON_CODE = "codigo";
    public static final String JSON_MESSAGE = "mensaje";
    public static final String JSON_ERROR = "codigo";
    private int code;
    private String message;
    private String error;
    private T data;

    public Response() {
        this.code = ResponseCode.OK;
    }

    public int getCode() {
        return code;
    }

    void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void makeError() {
        if (this.data instanceof JSONObject) {
            try {
                JSONObject data = (JSONObject) this.data;
                if(data.has(this.JSON_CODE)) this.code = ((JSONObject) this.data).getInt(this.JSON_CODE);
                if(data.has(this.JSON_MESSAGE)) this.message = ((JSONObject) this.data).getString(this.JSON_MESSAGE);
                if(data.has(this.JSON_ERROR)) this.error = ((JSONObject) this.data).getString(this.JSON_ERROR);
            } catch (JSONException e) {
                this.code = ResponseCode.CONVERSION_ERROR;
                this.message = "";
                this.error = e.getLocalizedMessage();
            }
        }
    }

    /**
     * This is meant to be used JUST in catch block
     * @param code Depend upon exception
     * @param error Exception error
     * @param message Display a message to user
     */
    public void makeError(int code, String error, String message) {
        this.code = code;
        this.error = error;
        this.message = message;
    }

    public boolean isOk() {
        return code == ResponseCode.OK;
    }

    public <U> Response<U> convert() {
        Response<U> response = new Response<>();
        response.code = this.code;
        response.message = this.message;
        response.error = this.error;
        return response;
    }

    @Override
    public String toString() {
        return "Response{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", error='" + error + '\'' +
                ", data=" + data +
                '}';
    }
}
