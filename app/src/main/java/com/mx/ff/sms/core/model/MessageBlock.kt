package com.mx.ff.sms.core.model

data class MessageBlock(
    val campaignCreationDate: String,
    val clicked: Int,
    val creationDate: CreationDate,
    val received: Int,
    val sended: Int,
    val totalMessages: Int
)