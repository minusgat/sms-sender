package com.mx.ff.sms.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mx.ff.sms.core.model.TextMessage;
import com.mx.ff.sms.view.activity.MainActivity;
import com.mx.ff.sms.R;

import java.util.ArrayList;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.vH> {

    private Context context;
    private Activity activity;
    private ArrayList<TextMessage> textMessageArrayList;
    private RecyclerView recyclerView;

    public MessagesAdapter(Context context, RecyclerView recyclerView) {
        this.context = context;
        this.activity = (Activity) context;
        this.textMessageArrayList = new ArrayList<>();
        this.recyclerView = recyclerView;
    }

    public ArrayList<TextMessage> getMessages() {
        return textMessageArrayList;
    }

    @Override
    public vH onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(context)
                .inflate(R.layout.item_message, parent, false);


        vista.setOnClickListener(view -> {
            int position = recyclerView.getChildAdapterPosition(vista);
            TextMessage acctualMessage = textMessageArrayList.get(position);
            ((MainActivity) activity).sendSMS(acctualMessage);
            textMessageArrayList.remove(position);
            notifyDataSetChanged();
        });

        return new vH(vista);
    }

    @Override
    public void onBindViewHolder(vH holder, int position) {
        final TextMessage textMessage = textMessageArrayList.get(position);
        holder.setDatos(textMessage);
    }

    @Override
    public int getItemCount() {
        return textMessageArrayList.size();
    }

    public void bindMessages(@NonNull ArrayList<TextMessage> textMessageArrayList) {
        this.textMessageArrayList.clear();
        if (textMessageArrayList.isEmpty()) {
            Toast.makeText(context, "Ningun resultado", Toast.LENGTH_SHORT).show();
        } else {
            this.textMessageArrayList.addAll(textMessageArrayList);
        }
        notifyDataSetChanged();
    }

    public void agregarInteres(TextMessage textMessage) {
        this.textMessageArrayList.add(textMessage);
        notifyDataSetChanged();
    }

    public TextMessage removeMessage() {
        if (textMessageArrayList.size() > 0) {
            TextMessage textMessage = textMessageArrayList.get(0);
            textMessageArrayList.remove(0);
            notifyDataSetChanged();
            return textMessage;
        }
        return new TextMessage();
    }

    class vH extends RecyclerView.ViewHolder {

        TextView messageTextView, phoneTextView, dateTextView;

        vH(View vista) {
            super(vista);
            phoneTextView = vista.findViewById(R.id.textView);
            messageTextView = vista.findViewById(R.id.textView2);
            dateTextView = vista.findViewById(R.id.textView3);

        }

        void setDatos(TextMessage causa) {
            phoneTextView.setText(causa.getNumber());
            messageTextView.setText(causa.getMessages());
            dateTextView.setText(causa.getFecha());
        }
    }
}
