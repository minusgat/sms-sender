package com.mx.ff.sms.core.ejb

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mx.ff.sms.core.model.Message
import com.mx.ff.sms.core.model.MessageBlock
import com.mx.ff.sms.core.model.RemoteMessagge
import com.mx.ff.sms.core.net.Routes.*
import com.mx.ff.sms.core.net.WebService
import com.mx.ff.sms.core.util.AsyncResponse
import com.mx.ff.sms.core.util.Response
import com.mx.ff.sms.core.util.ResponseCode
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.http.PATCH
import retrofit2.http.POST

class AlertsEJB(viewUpdater: AsyncResponse) : CustomEJB(viewUpdater) {
    private val TAG = AlertsEJB::class.java.simpleName



    fun sendAlert(id :String) {

        val data = JSONObject("{\"messages\":\n" +
                "[\n" +
                "\t{\n" +
                "\t\t\"message\":\" hola, soy Arturo y estoy sufriendo una crisis ahora, mi ubicación actual es esta\",\n" +
                "\t\t\"dn\":5514736210,\n" +
                "\t\t\"coordinates\":{\"lat\":19.693404633333333,\"lon\":-99.22348360000001},\n" +
                "\t\t\"time\":\"14 05 2020 0:50\",\n" +
                "\t\t\"email\": \"avillegas@impactar.mx\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"message\":\" hola, soy Arturo y estoy sufriendo una crisis ahora, mi ubicación actual es esta\",\n" +
                "\t\t\"dn\":5514736210,\n" +
                "\t\t\"coordinates\":{\"lat\":19.693404633333333,\"lon\":-99.22348360000001},\n" +
                "\t\t\"time\":\"14 05 2020 0:50\",\n" +
                "\t\t\"email\": \"avillegas@impactar.mx\"\n" +
                "\t}\n" +
                "]\n" +
                "\t\n" +
                "}\n")

        WebService(
                basicCallbck,
                POST_METHOD,
                "messages/campaign",
                "http://***********:8095/api/"
        ).execute(data)
    }

    val basicCallbck = AsyncResponse { response: Response<*> ->
        if (!response.isOk) response.makeError()
        if (validateViewUpdater()) viewUpdater.processFinish(response)
    }

}