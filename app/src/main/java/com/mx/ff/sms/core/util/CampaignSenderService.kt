package com.mx.ff.sms.core.util

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.telephony.SmsManager
import android.widget.Toast
import com.mx.ff.sms.R
import com.mx.ff.sms.core.ejb.CampaignsEJB
import com.mx.ff.sms.core.ejb.UserEJB
import com.mx.ff.sms.core.model.Message
import com.mx.ff.sms.view.activity.CloseActivity
import com.mx.ff.sms.view.activity.LoginActivity

class CampaignSenderService : Service() {
    private val CHANNEL_ID = "ForegroundService Kotlin"
    private var total = 0
    private var sended = 0
    private var received = 0
    private var attemps = 0
    private var campaign = ""
    private lateinit var messagesList: List<Message>
    private lateinit var campaignsListList: List<String>
    lateinit var recieverManager: ReceiverManager


    companion object {
        fun startService(context: Context, message: String) {
            val startIntent = Intent(context, CampaignSenderService::class.java)
            startIntent.putExtra("inputExtra", message)
            ContextCompat.startForegroundService(context, startIntent)
        }

        fun stopService(context: Context) {
            val stopIntent = Intent(context, CampaignSenderService::class.java)
            context.stopService(stopIntent)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        campaign = intent?.getStringExtra("inputExtra").toString()
        Sesion.getSesion().init(this)
        recieverManager= ReceiverManager.init(this)
        UserEJB(AsyncResponse {
            if (it.isOk) {
                Toast.makeText(this, "Sending", Toast.LENGTH_SHORT).show()
                createNotificationChannel()
                retriveMessages()
            } else Toast.makeText(this, "Ups! Something was wrong", Toast.LENGTH_SHORT).show()
        }).getToken(Sesion.getSesion().userName, Sesion.getSesion().password)
        //stopSelf();
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    fun sendSMS(message: Message) {

        if (message.sendDate.equals("Sin fecha") ) {
            val SENT = "SMS_SENT_" + message.publicId
            val DELIVERED = "SMS_DELIVERED_" + message.publicId

            val sentPI = PendingIntent.getBroadcast(this, 0,
                    Intent(SENT), 0)

            val deliveredPI = PendingIntent.getBroadcast(this, 0,
                    Intent(DELIVERED), 0)

            //---when the SMS has been sent---
            recieverManager.registerReceiver(object : BroadcastReceiver() {
                override fun onReceive(arg0: Context, arg1: Intent) {
                    attemps++
                    var status = ""
                    when (resultCode) {
                        Activity.RESULT_OK -> {
                            status = "Enviado"
                            sended++

                        }
                        SmsManager.RESULT_ERROR_GENERIC_FAILURE -> status = "Generic Failure"
                        SmsManager.RESULT_ERROR_NO_SERVICE -> status = "No Service"
                        SmsManager.RESULT_ERROR_NULL_PDU -> status = "Null PDU"
                        SmsManager.RESULT_ERROR_RADIO_OFF -> status = "Radio off"
                    }
                    CampaignsEJB(AsyncResponse {}).setMessageStatus(message.publicId, status)
                    refreshNotification()
                    recieverManager.unregisterReceiver(this)

                }
            }, IntentFilter(SENT))

            //---when the SMS has been delivered---
            recieverManager.registerReceiver(object : BroadcastReceiver() {
                override fun onReceive(arg0: Context, arg1: Intent) {
                    var status = ""
                    when (resultCode) {
                        Activity.RESULT_OK -> {
                            status = "Entregado"
                            received++

                        }
                        Activity.RESULT_CANCELED -> status = "No Entregado"
                    }
                    refreshNotification()
                    CampaignsEJB(AsyncResponse {}).setMessageStatus(message.publicId, status)
                    recieverManager.unregisterReceiver(this)
                }
            }, IntentFilter(DELIVERED))

            val sms = SmsManager.getDefault()

            sms.sendTextMessage(message.dn.toString(), null, message.message, sentPI, deliveredPI)
        }
    }

    fun refreshNotification() {
        val pendingIntent = PendingIntent.getActivity(this, 0, Intent(this, LoginActivity::class.java), 0)
        val closeIntent = PendingIntent.getActivity(this, 0, Intent(this, CloseActivity::class.java), 0)
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("SMS Masivos: $campaign")
                .setContentText("Total:$total Intentos:$attemps  Enviados:$sended Recibidos:$received Estado:")
                .setSmallIcon(R.drawable.check_icon)
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.close_icon, getString(R.string.stop_campaing),
                        closeIntent)
                .build()
        startForeground(1, notification)
    }

    fun sendCampaign(index: Int) {

        if (index >= messagesList.size ) return

        Handler().postDelayed({
            sendSMS(messagesList[index])
            sendCampaign(index + 1)

        }, 2000)
    }


    fun retriveMessages() {
        CampaignsEJB(AsyncResponse {
            if (it.isOk) {
                messagesList = (it.data as List<Message>).filter { el -> el.sendDate.equals("Sin fecha") }
                total = messagesList.size
                refreshNotification()
                sendCampaign(0)
            }
        }).retriveMessagesBlocksDetail(campaign)
    }


}