package com.mx.ff.sms.core.model

data class CreationDate(
    val longDate: String,
    val numeric: Long
)